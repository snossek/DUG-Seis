# DUG Seis
Software for acquisition and processing of micro-seismic data

## Status
This software has alpha/beta status. It cannot be expected to fulfill any specific requirements or to flawlessly fulfill any specific specific task. All liability derogated - use it at your own risk, or don't use it!


## Installation
1. download the latest version of the code from https://gitlab.seismo.ethz.ch/doetschj/DUG-Seis.git
2. install conda, if it is not already installed
3. add the conda forge channel to conda: "conda config --add channels conda-forge"
3. create a new conda environment using the command "conda create -n dug-seis python=3.6 obspy redis celery”. It is important to use python version 3.6, as there are some incompatibilities between different packages for 3.7.
4. move into the new environment using "conda activate dug-seis”
5. cd into the base folder of the dug-seis software
6. install the software using "pip install -e .” The “-e” in the install command makes sure that you can change the code and the changes will be directly effective, when you call dug-seis again. Once we are out of the immediate development stage, it makes sense to install using “pip install .”. Then you will have to re-install, when you changed the code or downloaded a new version.

## License & Copyright

DUG Seis is licensed under the GNU Lesser General Public License, Version 3
    (https://www.gnu.org/copyleft/lesser.html).

The copyright is held by ETH Zurich, Switzerland.
Main Contributors for the first version developed until August 2019 are
- Joseph Doetsch
- Thomas Haag
- Sem Demir
- Linus Villiger



