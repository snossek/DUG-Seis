clc
close all
clear all

% main_path = 'E:\Linus\1_Hydraulic_shearing\HS4\2_event_detection';
main_path = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection';


%% Log file import
filename = [main_path  '/01_Code/00_log/5_HF3_events_proc.csv'];
delimiter = ',';
startRow = 2;
formatSpec = '%f%f%s%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'EmptyValue' ,NaN,'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
Event_id = dataArray{:, 1};
Coincidence_sum = dataArray{:, 2};
Start = dataArray{:, 3};
End = dataArray{:, 4};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

formatIn = 'yyyy-mm-ddTHH:MM:SS.FFF';

Start_date_num = zeros(size(Event_id,1), 1); 
for i=1:size(Event_id,1)

date_vec = datevec(Start(i),formatIn);
Start_date_num(i) = datenum(date_vec);

end


%% Pulser file import
filename = [main_path  '/01_Code/00_log/2_Pulser_HF3.csv'];
delimiter = ',';
startRow = 2;
formatSpec = '%f%s%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
Survey = dataArray{:, 1};
Survey_Start = dataArray{:, 2};
Survey_End = dataArray{:, 3};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;


Survey_Start_date_num = zeros(size(Survey,1), 1); 
Survey_End_date_num = zeros(size(Survey,1), 1); 
for i=1:size(Survey,1)

pulser_vec_start = datevec(Survey_Start(i),formatIn);
Survey_Start_date_num(i) = datenum(pulser_vec_start);

date_vec_end = datevec(Survey_End(i),formatIn);
Survey_End_date_num(i) = datenum(date_vec_end);

end


%% Hammer file import
filename = [main_path  '/01_Code/00_log/1_Hammers_HF3.csv'];
delimiter = ',';
startRow = 2;
formatSpec = '%f%f%s%s%[^\n\r]';
fileID = fopen(filename,'r');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'HeaderLines' ,startRow-1, 'ReturnOnError', false);
fclose(fileID);
Hammer_Survey = dataArray{:, 1};
Hammer_nr = dataArray{:, 2};
Hammer_Start = dataArray{:, 3};
Hammer_End = dataArray{:, 4};
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

Hammer_Start_date_num = zeros(size(Survey,1), 1); 
for i=1:size(Hammer_nr,1)
hammer_vec_start = datevec(Hammer_Start(i),formatIn);
Hammer_Start_date_num(i) = datenum(hammer_vec_start);

end

%% Pressure & Flow rate import
filename = 'HF3.mat';
load(filename)

flow_pres_time_datenum = HF3.proc(:,1);
Injection_Pressure_MPa = HF3.proc(:,6);
cleaned_flow_lpm = HF3.proc(:,10);







%% Get datenum of start time

start_plot = 13.5;
end_plot = 16.00;

coinc_8 = find(Coincidence_sum == 8);
coinc_7 = find(Coincidence_sum == 7);
coinc_6 = find(Coincidence_sum == 6);
coinc_5 = find(Coincidence_sum == 5);
coinc_4 = find(Coincidence_sum == 4);
coinc_3 = find(Coincidence_sum == 3);
coinc_2 = find(Coincidence_sum == 2);


coinc_8_start = (Start_date_num(coinc_8)-floor(Start_date_num(1))).*24;
coinc_7_start = (Start_date_num(coinc_7)-floor(Start_date_num(1))).*24;
coinc_6_start = (Start_date_num(coinc_6)-floor(Start_date_num(1))).*24;
coinc_5_start = (Start_date_num(coinc_5)-floor(Start_date_num(1))).*24;
coinc_4_start = (Start_date_num(coinc_4)-floor(Start_date_num(1))).*24;
coinc_3_start = (Start_date_num(coinc_3)-floor(Start_date_num(1))).*24;
coinc_2_start = (Start_date_num(coinc_2)-floor(Start_date_num(1))).*24;


bin_width = 1/(3600*2); % half a second
edges = start_plot:bin_width:end_plot;
h_8 = histcounts(coinc_8_start,edges);
h_cum_sum_8 = cumsum(h_8);
h_7 = histcounts(coinc_7_start,edges);
h_cum_sum_7 = cumsum(h_7);
h_cum_sum_78 = h_cum_sum_7 + h_cum_sum_8;
h_6 = histcounts(coinc_6_start,edges);
h_cum_sum_6 = cumsum(h_6);
h_cum_sum_678 = h_cum_sum_6 + h_cum_sum_78;
h_5 = histcounts(coinc_5_start,edges);
h_cum_sum_5 = cumsum(h_5);
h_cum_sum_5678 = h_cum_sum_5 + h_cum_sum_678;
h_4 = histcounts(coinc_4_start,edges);
h_cum_sum_4 = cumsum(h_4);
h_cum_sum_45678 = h_cum_sum_4 + h_cum_sum_5678;
h_3 = histcounts(coinc_3_start,edges);
h_cum_sum_3 = cumsum(h_3);
h_cum_sum_345678 = h_cum_sum_3 + h_cum_sum_45678;
h_2 = histcounts(coinc_2_start,edges);
h_cum_sum_2 = cumsum(h_2);
h_cum_sum_2345678 = h_cum_sum_2 + h_cum_sum_345678;


h = [h_cum_sum_8;h_cum_sum_7;h_cum_sum_6; h_cum_sum_5; h_cum_sum_4; h_cum_sum_3; h_cum_sum_2];
time = (start_plot+bin_width/2):bin_width:(end_plot-bin_width/2);


y_lim_hammer = interp1(time,h_cum_sum_2345678,(Hammer_Start_date_num-floor(Start_date_num(1))).*24);
y_lim_pulser_start = interp1(time,h_cum_sum_2345678, (Survey_Start_date_num-floor(Start_date_num(1))).*24);
y_lim_pulser_end = interp1(time,h_cum_sum_2345678,(Survey_End_date_num-floor(Start_date_num(1))).*24);


fig = figure;

axis_color = [0 0 0];
set(fig,'defaultAxesColorOrder',[axis_color; axis_color]);

yyaxis left
flow = plot((flow_pres_time_datenum-floor(flow_pres_time_datenum(1))).*24, cleaned_flow_lpm, 'b','linewidth',1.5);
hold on
pres = plot((flow_pres_time_datenum-floor(flow_pres_time_datenum(1))).*24, Injection_Pressure_MPa, 'r','linestyle','-', 'linewidth',1.5);
set(gca, 'YColor', [0 0 0]);
ylabel('injection pressure [MPa], flow rate [l/min]')
ylim([-10 90])
xlim([13.5 16])
hold on
grid on
grid minor

yyaxis right
strip_off = 15;
strip_length = 100;
for i=1:size(Survey ,1)
line([(Survey_Start_date_num(i)-floor(Start_date_num(1))).*24 (Survey_Start_date_num(i)-floor(Start_date_num(1))).*24],[y_lim_pulser_start(i)+strip_off y_lim_pulser_start(i)+ strip_length], 'color', 'black')
hold on
line([(Survey_End_date_num(i)-floor(Start_date_num(1))).*24 (Survey_End_date_num(i)-floor(Start_date_num(1))).*24],[y_lim_pulser_end(i)+strip_off y_lim_pulser_end(i)+ strip_length], 'color','black')
hold on
end

% Hammer plot
for i=1:size(Hammer_nr ,1)
line([(Hammer_Start_date_num(i)-floor(Start_date_num(1))).*24 (Hammer_Start_date_num(i)-floor(Start_date_num(1))).*24],[y_lim_hammer(i)+strip_off y_lim_hammer(i)+strip_length],'Color',[0.6 0.6 0.6])
hold on

end

cum_events = line(time, h_cum_sum_2345678, 'color', 'k', 'linewidth', 2);
hold on
line(time, h_cum_sum_345678, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on
line(time, h_cum_sum_45678, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on
line(time, h_cum_sum_5678, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on
line(time, h_cum_sum_678, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on
line(time, h_cum_sum_78, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on
line(time, h_cum_sum_8, 'color', 'k', 'linewidth', 0.5, 'linestyle', '--');
hold on


harea = area(time, transpose(h));
alpha(.2)

grid on
grid minor
title('Event evolution during experiment HF3, 16.05.2017')
ylabel('cumulative number of events []')
xlabel('time of day [h]')
ylim([-250 2500])
set(gca,'YTick',0:250:2500);
hold on
text_pos = 15.85;
text_off = 10;
% text(text_pos,h_cum_sum_8(end)-text_off,'coinc. 8')
% text(text_pos,h_cum_sum_78(end)-text_off,'coinc. 7')
% text(text_pos,h_cum_sum_678(end)-text_off,'coinc. 6')
% text(text_pos,h_cum_sum_5678(end)-3.5,'coinc. 5')
% text(text_pos,h_cum_sum_45678(end)-6.5,'coinc. 4')
text(text_pos,h_cum_sum_345678(end)-35,'coinc. 3')
text(text_pos,h_cum_sum_2345678(end)-35,'coinc. 2')



hold on

yyaxis right; ylimr = get(gca,'Ylim');ratio = ylimr(1)/ylimr(2);

yyaxis left; yliml = get(gca,'Ylim');

if yliml(2)*ratio<yliml(1)
    set(gca,'Ylim',[yliml(2)*ratio yliml(2)])
else
    set(gca,'Ylim',[yliml(1) yliml(1)/ratio])
end

hold on
legend_line = plot(10.5,30,'w');

tot_per_coinc = plot(10.5,30,'w');
coinc_8_legend = plot(10.5,30,'w');
coinc_7_legend = plot(10.5,30,'w');
coinc_6_legend = plot(10.5,30,'w');
coinc_5_legend = plot(10.5,30,'w');
coinc_4_legend = plot(10.5,30,'w');
coinc_3_legend = plot(10.5,30,'w');
coinc_2_legend = plot(10.5,30,'w');















legend([cum_events flow pres, legend_line, tot_per_coinc, coinc_8_legend, coinc_7_legend, coinc_6_legend, coinc_5_legend, coinc_4_legend, coinc_3_legend, coinc_2_legend], 'cumulative number of events', 'flow rate', 'injection pressure',  '-----------------------------------------','Total events per coinc. sum:',  ['2: ' num2str(size(coinc_2,1))], ['3: ' num2str(size(coinc_3,1))], ['4: ' num2str(size(coinc_4,1))] ,['5: ' num2str(size(coinc_5,1))], ['6: ' num2str(size(coinc_6,1))], ['7: ' num2str(size(coinc_7,1))], ['8: ' num2str(size(coinc_8,1))], 'Location','northwest')


set(findall(gcf,'-property','FontSize'),'FontSize',16)








%% Right hand side


% tightfig;
% figure
% area(time, h_cum_sum_8, 'FaceColor', [0 0 0])
% alpha(0.25)
% hold on
% plot(time, h_cum_sum_8, 'k', 'linewidth', 0.5)
% hold on
% 
% area(time, h_cum_sum_78, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_78, 'k', 'linewidth', 0.5)
% hold on
% 
% area(time, h_cum_sum_678, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_678, 'k', 'linewidth', 0.5)
% 
% area(time, h_cum_sum_5678, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_5678, 'k', 'linewidth', 0.5)
% 
% area(time, h_cum_sum_45678, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_45678, 'k', 'linewidth', 0.5)
% 
% area(time, h_cum_sum_345678, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_345678, 'k', 'linewidth', 0.5)
% 
% area(time, h_cum_sum_2345678, 'FaceColor', [0 0 0])
% alpha(0.1)
% hold on
% plot(time, h_cum_sum_2345678, 'k', 'linewidth', 2)
% 



% figure
% fill(time,h_cum_sum_2345678,'r');
% plot(time, h_cum_sum_2345678,'k', 'linewidth',2)
% 
% figure
% time = (10+bin_width/2):bin_width:(17-bin_width/2);
% plot(time, h_cum_sum_8, time, h_cum_sum_78, time, h_cum_sum_678, time, h_cum_sum_5678)
% 
