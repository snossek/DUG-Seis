#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 27 14:42:21 2017

@author: linus
"""

import os
import numpy as np
import pandas as pd
## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log' # Location of log file
#data_path_4 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/5_events'
#data_path_5 = '/home/linus/ETHZ/09_Event_detection/1_getting_active_seismics/5_electronic_noise'

#os.chdir(data_path_2)
#from plot_waveform_v01 import plot_waveform

## get master evenet list
os.chdir(data_path_1)
event_list = pd.read_csv('5_HF3_events_proc.csv')
Event_id = event_list['Event_id'].tolist()
Start = event_list['Start'].tolist()
End = event_list['End'].tolist()
Coincidence_sum = event_list['Coincidence_sum'].tolist()

ind_dict = {}
ind_dict['8'] = np.where(np.asarray(Coincidence_sum) == 8)
ind_dict['7'] = np.where(np.asarray(Coincidence_sum) == 7)
ind_dict['6'] = np.where(np.asarray(Coincidence_sum) == 6)
ind_dict['5'] = np.where(np.asarray(Coincidence_sum) == 5)
ind_dict['4'] = np.where(np.asarray(Coincidence_sum) == 4)
ind_dict['3'] = np.where(np.asarray(Coincidence_sum) == 3)
ind_dict['2'] = np.where(np.asarray(Coincidence_sum) == 2)



#ind_8 = np.where(np.asarray(Coincidence_sum) == 8)
#ind_7 = np.where(np.asarray(Coincidence_sum) == 7)
#ind_6 = np.where(np.asarray(Coincidence_sum) == 6)
#ind_5 = np.where(np.asarray(Coincidence_sum) == 5)
#ind_4 = np.where(np.asarray(Coincidence_sum) == 4)
#ind_3 = np.where(np.asarray(Coincidence_sum) == 3)
#ind_2 = np.where(np.asarray(Coincidence_sum) == 2)

# Get folder content to csv
coinc_nr = np.linspace(2, 8, num=7)
for x in enumerate(coinc_nr):


    file_name = '6_coin_' + str(int(x[1])) + '.csv'
               
    event_id = np.empty([ind_dict[str(int(x[1]))][0].shape[0],1])
    start_time = []
    end_time = []
    coincidence_sum = []
    cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')
    
    ind = ind_dict[str(int(x[1]))][0]
    
    for i in range(len(ind)):              
        event_id = int(Event_id[ind[i]])
    #    Event_id = list(map(int, Event_id))
        time_start = Start[ind[i]]
        time_end = End[ind[i]]
        sum_coincidence = Coincidence_sum[ind[i]]
    #    start_time.append(time_start)
    #    end_time.append(time_end)
    #    coincidence_sum.append(sum_coincidence) 
        
        data = [event_id] + [sum_coincidence] + [time_start] + [time_end]
        df = pd.DataFrame(data = [data], columns = cols)
        #
        if os.path.exists(file_name):
            df.to_csv(file_name, mode='a', header=False, index = False)
        else:
            df.to_csv(file_name,index = False)

#df = pd.read_csv(log_file)
#start_time_df = df["Start"].tolist()
#end_time_df = df["End"].tolist()
#event_id_df = df["Event_id"].tolist()
#coincidence_sum_df = df["Coincidence_sum"].tolist()