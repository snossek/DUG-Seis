def get_hammer_nr(dataStream,hammerhit_start):

    from obspy.signal.trigger import recursive_sta_lta, trigger_onset
    import numpy as np
    parm_STA_LTA = np.loadtxt('parm_STA_LTA_hammers.dat',delimiter=' ')


#    hammer_nr = np.ones(8)
#    hammerhit_start = np.empty(8)

    def get_trig(trace, sta, lta, on, off):
        cft = recursive_sta_lta(trace.data, int(sta), int(lta))    
        trig = trigger_onset(cft,on,off)
        return trig
     
    trig_time = np.empty(11) * np.nan
    trig_time_max = np.empty(11) * np.nan
    #    ind_a_max = np.zeros(11)
    trig_np = []
    T = (1/dataStream[0].stats.sampling_rate)
    for f in range(len(dataStream)):# check until HS10
        if np.isnan(parm_STA_LTA[f][0]): # Checks if nan in parm _STA_LTA
            pass
        else:
            trig_np = get_trig(dataStream[f], parm_STA_LTA[f][0], parm_STA_LTA[f][1], parm_STA_LTA[f][2], parm_STA_LTA[f][3])
            trig_time_max_np = np.argmax(dataStream[f].data)
        
        if trig_np == []:
            pass
        else:
            # From samples to relative arrival time in ms
            trig_time[f] = np.multiply(trig_np[0,0],(1/dataStream[0].stats.sampling_rate))*1000
            trig_time_max[f] = (trig_time_max_np * T)*1000
        # Get maximum of data stream
    
    # "Hammer finger prints"
    H = np.ones((8,10))
    H[0,:] = [4.505,1.41,18.65,39.255,11.775,14.165,12.55,12.28,11.085,10.965]
    H[1,:] = [10.075,3.685,3.15,9.825,7.915,11.28,10.41,9.905,9.83,9.735]
    H[2,:] = [8.735,6.725,4.435,2.8,4.44,9.615,8.59,9.2,9.315,9.735]
    H[3,:] = [14.555,8.97,10.755,3.145,2.295,7.39,7.59,8.245,10.21,10.34]
    H[4,:] = [15.335,12.84,10.39,7.94,6.59,2.325,1.96,6.725,10.995,7.565]
    H[5,:] = [14.335,11.96,9.16,7.965,6.64,6.63,2.57,2.085,6.685,5.875]
    H[6,:] = [13.68,11.665,11.5,10.105,10.475,11.22,7.26,2.345,2.72,7.31]
    H[7,:] = [12.925,11.69,9.645,9.74,10.62,16.49,11.84,7.125,2.395,1.68]
    
    # Duration of hammerhit [s]
    H_d = np.ones(8)
    H_d[0] = 0.8
    H_d[1] = 0.8
    H_d[2] = 0.8
    H_d[3] = 0.8
    H_d[4] = 0.6
    H_d[5] = 0.4
    H_d[6] = 0.15
    H_d[7] = 0.7

    diff_H = np.ones(8)
    for l in range(8):
      diff_H[l] = sum(abs(trig_time_max[1:] - H[l,:]))
        
    hammer_nr = np.argmin(diff_H)+1
    hammerhit_end = hammerhit_start + H_d[(hammer_nr)-1]    
#    print(hammer_nr)

    return hammer_nr, hammerhit_end