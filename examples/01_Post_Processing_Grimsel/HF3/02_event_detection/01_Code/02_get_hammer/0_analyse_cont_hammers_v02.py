#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
from a_get_hammers_invest_v03 import get_hammers
from b_get_hammer_nr_endtime_v03_argmax import get_hammer_nr
#import matplotlib.pyplot as plt


import pandas as pd

log_file = '1_Hammers_HF3.csv'
survey = 0
# HS4 start: 09.02.2017; 10:20:01.226000
# end: 09.02.2017; 10:36:47.858955

# print(dat_1.waveforms.GRM_001.raw_recording)


## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)



os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170516_HF1_HF3.h5")
os.chdir(data_path_2)
# time_start = UTCDateTime(2017, 5, 18, 12, 30, 00, 000000)
time_start = UTCDateTime(2017, 5, 16, 13, 40, 0, 000000)
delta_load = 30# Time in s
time_delta = time_start + delta_load
time_end = UTCDateTime(2017, 5, 16, 16, 00, 0, 000000) # Endtime exp. HS4
#time_end = UTCDateTime(2017, 5, 18, 16, 30, 0, 000000) # Endtime exp. HS4
 
## End Initialisation


# Hammer signal reverence
hammer_rev = np.load('hammer_signal.npy')
#np.save('hammer_signal.npy', dat_2[0].data)

#ch_in = np.linspace(1, 32, num=32)
#ch_in = [1]
#ch_in = np.linspace(1, 11, num=11)

ch_in = [1]
num = np.zeros(32)
hammerhit_start = []
while (time_delta < time_end):
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_delta,
                            tag="raw_recording")
                

    hammerhit_start += get_hammers(dat_2) 
    
    print(time_delta)
    time_start += delta_load
    time_delta += delta_load
#    dat_2.plot(size=(800, 600))
    
    
    del dat_2
    

## 2. Corelate hammerhit signal
cor_hammer_mask = np.ones((len(hammerhit_start), 1), dtype=bool)
for i in range(len(hammerhit_start)):
    time_start = hammerhit_start[i]
    delta = 0.00025 # Time in s
    time_end = time_start + delta
    
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_end,
                            tag="raw_recording")
        
#    plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', trig_sample)
    
    # Plotting the sample hammer signal
#    samp_v = np.arange(0,len(hammer_rev),1)
#    plt.plot(samp_v,hammer_rev)    
        
    # Here we check if the signal to compare, have the same size, if not --> trimmed
    hammer_actual = dat_2[0].data
    if len(hammer_actual) > len(hammer_rev):
        hammer_actual = dat_2[0].data[0:len(hammer_rev)]
    elif len(hammer_actual) < len(hammer_rev):
        hammer_rev = hammer_rev[0:len(dat_2[0].data)]

    hammer_corr = np.corrcoef(hammer_rev,hammer_actual)[0,1]


    if hammer_corr < 0.80:
#        del hammerhit_start[i]
#        i = i - 1 # Need to take one off, cause one deleted
        print('hammer deleted', hammerhit_start[i], '; hammer_corr:', hammer_corr)
        hammerhit_start[i] = 'nan' 

## Delets all "NaN" in hammer_hit_start vector and you'll be fine...;)
hammerhit_start = [x for x in hammerhit_start if str(x) != 'nan']        
    
## 3. We declare hammerhits
ch_in = np.linspace(1, 11, num=11) # I only want to look at channel 2 to 11
#ch_in = [1,2]
print(len(hammerhit_start))
for i in range(len(hammerhit_start)):
    time_start = hammerhit_start[i]
    delta = 0.05 # Time in s
    time_end = time_start + delta
    pre_trig = -0.000
    
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start-pre_trig, 
                            endtime=time_end,
                            tag="raw_recording")
                
#    plot_waveform(dat_2,dat_2[0].stats.sampling_rate,'test')
    hammer_nr, hammerhit_end = get_hammer_nr(dat_2, hammerhit_start[i])
    del dat_2
    
    if hammer_nr == 1:
        survey += 1
    
    cols = pd.Index(['Survey','Hammer','Start','End'],name='cols')
    data = [survey] + [hammer_nr] + [hammerhit_start[i].isoformat()] + [hammerhit_end.isoformat()]
    df = pd.DataFrame(data = [data], columns = cols)
    
    
    os.chdir(data_path_2)
    if os.path.exists(log_file):
        df.to_csv(log_file, mode='a', header=False,index = False)
    else:
        df.to_csv(log_file,index = False)
    
    os.chdir(data_path_2)       
        
        
