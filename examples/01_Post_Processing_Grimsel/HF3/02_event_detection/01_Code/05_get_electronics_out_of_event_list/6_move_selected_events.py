#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  1 13:32:59 2017

@author: linus
"""

import numpy as np
#import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
#from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta
import pandas as pd
import shutil

## Experiment
exp = 'HF8'

data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'
data_path_3 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/' + exp + '/2_event_detection/02_Data/01_events/01_events'
data_path_4 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/' + exp + '/2_event_detection/02_Data/01_events/01.1_events_sorted'
if not os.path.exists(data_path_4):
    os.makedirs(data_path_4)




os.chdir(data_path_2)
log_file = '4_Events_' + exp + '_coinc3_ev_10_11_16_23.csv'
df = pd.read_csv(log_file)
start_time_df = df["Start"].tolist()
end_time_df = df["End"].tolist()
event_id_df = df["Event_id"].tolist()
coincidence_sum_df = df["Coincidence_sum"].tolist()


os.chdir(data_path_3)



# Move files
for i in range(len(event_id_df)):
        print(i, '/',len(event_id_df))
        file_liest = str(event_id_df[i]).zfill(5) + '_' + str(coincidence_sum_df[i]) + '_' + start_time_df[i][11:13] + start_time_df[i][14:16] + start_time_df[i][17:19] + '_' + start_time_df[i][20:26] + '.' + 'segy'
        shutil.move(file_liest, data_path_4)
        

# 00001_5_122959_488315.segy