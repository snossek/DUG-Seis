
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 26 17:10:54 2017

@author: linus
"""

import numpy as np
#import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
#from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta
#from obspy.signal.trigger import plot_trigger
#from obspy.signal.trigger import classic_sta_lta
#from d_pick_events_electronic import pick_events_electronic
#from pprint import pprint
import pandas as pd
from f_pick_electronics_v06 import pick_electric


## Experiment
exp = 'HF3'

## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log'

data_path_3 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_' + exp + '/2_event_detection/02_Data/01_events/01_events'
if not os.path.exists(data_path_3):
    os.makedirs(data_path_3)

data_path_4 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_' + exp + '/2_event_detection/02_Data/01_events/02_electronics'
if not os.path.exists(data_path_4):
    os.makedirs(data_path_4)


os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170516_HF1_HF3.h5")
os.chdir(data_path_2)
time_start = UTCDateTime(2017, 5, 16, 13, 40, 0, 000000)
delta_load = 30# Time in s
time_delta = time_start + delta_load
time_end = UTCDateTime(2017, 5, 16, 16, 00, 0, 000000)   

## End Initialisation

log_file_ev = '4_Events_' + exp + '_coinc_2_ev.csv'
log_file_el = '4_Events_' + exp + '_coinc_2_el.csv'
event_nr = 0
event_nr_el = 0

## Get event times
log_file = '3_Events_' + exp + '_coinc2_11_16_22.csv'

os.chdir(data_path_2)
df = pd.read_csv(log_file)
start_time_df = df["Start"].tolist()
end_time_df = df["End"].tolist()
event_id_df = df["Event_id"].tolist()
coincidence_sum_df = df["Coincidence_sum"].tolist()

start_time = [None] * len(start_time_df)
end_time = [None] * len(end_time_df)
for i in range(len(start_time_df)):
    start_time[i] = UTCDateTime(start_time_df[i])
    end_time[i] = UTCDateTime(end_time_df[i])


#station = np.linspace(1,32,32)
#station = station.astype(int)
#station = np.char.mod('%d' station)
#station = station.zfill(2)

# Pulser data: "2017-02-09T10:22:02.887"; "2017-02-09T10:22:02.92"
# First hammerhit: "2017-02-09T10:20:16.0055"; "2017-02-09T10:20:16.008"  
# GMuG continuous recording start at 09.02.2017; 10:20:01,226

# First hammer blow: [UTCDateTime(2017, 2, 9, 10, 20, 16, 6315)]
#len(start_time)
for f in range(len(start_time)):
    print(f)

    
    time_start = start_time[f]
    time_end = end_time[f] # Time in 0.05s
    
    # time_end = "2017-02-09T10:22:20" (beginning of pulser run)
    
    
    #[1,16,17,18,19,20,21,22,23]
#    ch_in = np.linspace(16, 23, num=8)
    ch_in = [1,11,16,17,18,19,20,21,22]

    
    num = np.zeros(8)
    dat_2 = Stream()
    for k in range(len(ch_in)):
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start, 
                            endtime=time_end,
                            tag="raw_recording")
#        print(dat_2)
    
    #dat_2.plot(color='green', tick_format='%I:%M %p', equal_scale= "false" )
    #
    dat_2_fd = dat_2
    print(len(dat_2),len(dat_2_fd))
    dat_2_fd.filter("bandpass", freqmin=1000.0, freqmax=15000.0)  
    pick_electric(dat_2,dat_2_fd, data_path_2, data_path_3, data_path_4, time_start, time_end, event_id_df[f], coincidence_sum_df[f],log_file_ev,log_file_el)
    del dat_2_fd, dat_2

    
#def pick_electric(dataStream,data_path_3, time_start, time_end, event_nr, event_nr_el, coins_sum,log_file,log_file_el):

    
    
    
    
    
#    plot_waveform(dat_2, dat_2[0].stats.sampling_rate, start_time_df[ev_o_i])


#trig_time = pick_events_electronic(dat_2,data_path_3)



##
#dat_2.plot(color='green', 
#           tick_format='%I:%M %p', 
#           starttime=UTCDateTime(time_start), 
#            endtime=UTCDateTime("2017-02-09T10:20:16.007"))



#
#
### Getting hammer
#from obspy.signal.trigger import trigger_onset
#
#time_in = 1 #[s]
#trigger_on = 5000
#trigger_off = 5000
#
#on_of = trigger_onset(dat_2[0].data, trigger_on, trigger_off)
#on_of = on_of.flatten() # puts vector in single dimension
#
#
### Checks if hammerhit is in interval of samples
#on_of_diff = np.diff(on_of)
#on_of_diff_bol = (on_of_diff > 80) & (on_of_diff < 150) # Max sample diff of pulses
#on_of_diff_bol = np.append([on_of_diff_bol],[np.zeros((1,1), dtype=bool)])
#on_of = on_of[on_of_diff_bol]
#
#
## Get trigger times ((x-1) because the 1 sample the start time)
#trig_times = [dat_2[0].stats.starttime + (x-1)*1/dat_2[0].stats.sampling_rate for x in on_of]
#
### Get slope of onsets by getting the sample point of one sampe later (later - present)
##on_of_slope = dat_2[0].data[(on_of[1:(on_of.shape[0]-1)]+1)] - dat_2[0].data[on_of[1:(on_of.shape[0]-1)]]
##on_of_slope_1 = (on_of_slope < 0) # Checks if slope is bellow 0, for falling slope we need below 0
### on_of_1 = on_of[1:(on_of.shape[0]-1)] * on_of_slope_1 
##on_of_1 = on_of[on_of_slope_1]
#         
### Distinguish between hammers






                
#T = 1/dat_2[0].stats.sampling_rate
#on_of = on_of * T


#if on_of:
#    fig = plt.figure()
#    tr_0 = dat_2[0]
#    #samp_v = np.arange(0,tr_0.data.shape[0],1)*1/tr_0.stats.sampling_rate*time_in
#    samp_v = np.arange(0,tr_0.data.shape[0],1)
#    plt.plot(samp_v,tr_0.data)
#    plt.xlabel('relative time [s]', fontsize=12)
#    plt.ylabel('amplitude [mV]', fontsize=12)
#    plt.title(tr_0.stats.starttime, fontsize=14)
#    plt.grid()
#    axes = plt.gca()
#    #
#    #for i in range(on_of.shape[0]):
#    plt.plot((on_of, on_of) , (axes.get_ylim()[0], axes.get_ylim()[1]), 'r-')
#    
#    plt.show()