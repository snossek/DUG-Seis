#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 30 10:46:14 2017

@author: linus
"""

import numpy as np
import math
from obspy.core import Stream, Trace, UTCDateTime
import os
import pyasdf
from tqdm import tqdm

data_path_1 = '/media/linus/ISC_AE_3/2_Hydraulic_fracturing_experiments/20170516_HF1_HF3' # Location of .dat files
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Where to save the .h5 files
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)


os.chdir(data_path_1)
dat_files = sorted([f for f in os.listdir(data_path_1) if f.endswith('.dat')])

s_rate = 200000 # Sampling rate in Hz
stats = {'network': 'GRM', 'station':'001', 'location': '001',
         'channel': '001', 'npts': 10, 'sampling_rate': s_rate,
         'mseed': {'dataquality': 'Q'}}
stats['starttime'] = '2017-05-16T08:45:00.174000Z' # Recording time of first data point
start_time = stats['starttime']



for k in tqdm(range(int(len(dat_files)/2))):

## Channel 1-16
    f = open(dat_files[k],'rb')
    dat_11 = f.read() # imports everything as a string
    dat_12 = np.fromstring(dat_11, dtype=np.int16, count =-1) #decods all, numbers in 2**16 bit digit steps
    size = math.floor(len(dat_12)/16) # converts len(y) back to integer
    dat_12 = np.reshape(dat_12,(size,16)) #Changed (16,size) to (size,16)
    del dat_11
    dat_12 = np.require(dat_12, dtype="int32")
    stats['npts'] = dat_12.shape[0]

    # Channel order [1,9,2,10,3,11,4,12,5,13,6,14,7,15,8,16]
    # Python order [0,8,1,9,2,10,3,11,4,12,5,13,6,14,7,15]
    # Matlab order [1,3,5,7,9,11,13,15,2,4,6,8,10,12,14,16]
    n_shape = [0,2,4,6,8,10,12,14,1,3,5,7,9,11,13,15]
    dat_12 = dat_12[:,n_shape]
    stream = Stream()
    
    for i in range(16):
        stats['station'] = str(i+1).zfill(3)
        stream += Trace(dat_12[:,i], header=stats)
    del dat_12
    
## Channel 17-32
    f = open(dat_files[int(len(dat_files)/2)+k],'rb')
    dat_21 = f.read() # imports everything as a string
    dat_22 = np.fromstring(dat_21, dtype=np.int16, count =-1) #decods all, numbers in 2**16 bit digit steps
    size = math.floor(len(dat_22)/16) # converts len(y) back to integer
    dat_22 = np.reshape(dat_22,(size,16)) #Changed (16,size) to (size,16)
    del dat_21
    dat_22 = np.require(dat_22, dtype="int32")
    stats['npts'] = dat_22.shape[0]

    # Channel order [1,9,2,10,3,11,4,12,5,13,6,14,7,15,8,16]
    # Python order [0,8,1,9,2,10,3,11,4,12,5,13,6,14,7,15]
    # Matlab order [1,3,5,7,9,11,13,15,2,4,6,8,10,12,14,16]
    n_shape = [0,2,4,6,8,10,12,14,1,3,5,7,9,11,13,15]
    dat_22 = dat_22[:,n_shape]

    for i in range(17,33):
        stats['station'] = str(i).zfill(3)
        stream += Trace(dat_22[:,(i-17)], header=stats)
    del dat_22
    
    stats['starttime'] = UTCDateTime(stream[0].stats.endtime) + 1/s_rate
    
## Writing the .h5 file
    os.chdir(data_path_2)
    dat_3 = pyasdf.ASDFDataSet("20170516_HF1_HF3.h5", compression="gzip-3")
    dat_3.append_waveforms(stream, tag="raw_recording")
    os.chdir(data_path_1)
    end_time = stream[0].stats.endtime
    del stream

