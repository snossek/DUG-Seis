#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 28 18:18:58 2017

@author: linus
"""
import numpy as np
#import matplotlib.pyplot as plt
from obspy import read
from obspy.core import Stream, Trace, UTCDateTime, read
import os
import pyasdf
#from tqdm import tqdm
from obspy.signal.trigger import coincidence_trigger, recursive_sta_lta
from obspy.signal.trigger import plot_trigger
from obspy.signal.trigger import classic_sta_lta
import pandas as pd

## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/02_HF3/2_event_detection/01_Code/00_log'


exp = 'HF3'
log_file = '3_Events_' + exp + '_coinc2_11_16_22.csv'


os.chdir(data_path_2)
log_file_read = '3_Events_' + exp + '_coinc2_11_16_22_incl_duplicates' + '.csv'
df = pd.read_csv(log_file_read)
start_time_df = df["Start"].tolist()
end_time_df = df["End"].tolist()
event_id_df = df["Event_id"].tolist()
coincidence_sum_df = df["Coincidence_sum"].tolist()

for i in range(len(start_time_df)):
    start_time_df[i] = UTCDateTime(start_time_df[i])

start_time, start_time_indices = np.unique(start_time_df, return_index=True)    
    
start_time = list(start_time)
end_time = [end_time_df[i] for i in start_time_indices]    
event_id = [event_id_df[i] for i in start_time_indices] 
coincidence_sum = [coincidence_sum_df[i] for i in start_time_indices] 


cols = pd.Index(['Event_id','Coincidence_sum','Start','End'],name='cols')

for f in range(len(start_time)):
    print(f)
    data = [event_id[f]] + [coincidence_sum[f]] + [start_time[f].isoformat()] + [end_time[f]]
    df = pd.DataFrame(data = [data], columns = cols)
    os.chdir(data_path_2)

    if os.path.exists(log_file):
        df.to_csv(log_file, mode='a', header=False, index = False)
    else:
        df.to_csv(log_file,index = False)