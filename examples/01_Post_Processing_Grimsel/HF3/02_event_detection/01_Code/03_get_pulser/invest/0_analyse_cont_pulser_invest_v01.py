#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
import matplotlib.pyplot as plt
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
from c_get_pulser import get_pulser
import pandas as pd
from plot_waveform_pulser_invest_v01 import plot_waveform


## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)

os.chdir(data_path_2)
# Pulser signal reverence
pulser_rev = np.load('pulser_signal_02s_before.npy')

#np.save('pulser_signal.npy', dat_2[0].data)  
    
#UTCDateTime(2017, 5, 18, 12, 45, 43, 942400)    
#UTCDateTime(2017, 5, 18, 12, 35, 48, 729785)
#UTCDateTime(2017, 5, 18, 12, 35, 43, 711585)    
#UTCDateTime(2017, 5, 18, 12, 35, 12, 597820)    
    
#time = '2017-05-18T13:05:13.274650Z'
time = '2017-05-18T16:25:17.200000'
time_before = 0.2
time = UTCDateTime(time)  -   time_before
    

time_end = '2017-05-18T16:27:00.000000'
time_end = UTCDateTime(time_end)     

os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170518_HF6_HF8.h5")
os.chdir(data_path_2)

time_start = time
delta_load = 0.2 # Time in s
time_delta = time_start + delta_load
#time_end = UTCDateTime(2017, 5, 18, 13, 59, 0, 000000) # Endtime exp. HS4
## End Initialisation

log_file = '2_Pulser_HF8.csv'

#ch_in = np.linspace(1, 32, num=32)
ch_in = [1]

num = np.zeros(32)
dat_2 = []
pulser = []
pulser_start = []
pulser_end = []
pulser_raw = []
l = 0
#while (time_delta < time_end):
dat_2 = Stream()
for k in range(len(ch_in)): 
    dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                      location="001", channel="001",
                      starttime=time_start, 
                        endtime=time_end,
                        tag="raw_recording")
            


        
[pulser, pulser_start] = get_pulser(dat_2)       
pulser_raw.append(pulser)

#print(time_delta)
time_start += delta_load
time_delta += delta_load
l += l + 1 

      
#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser)
#print(pulser_start[0])        
#dat_2.plot()
        





## 2. Corelate hammerhit signal

#for i in range(len(pulser_start)):
#time_start = pulser_start[2]
time_start = time
delta = 3 # Time in s
time_end = time_start + delta

dat_2 = Stream()
for k in range(len(ch_in)): 
    dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                      location="001", channel="001",
                      starttime=time_start, 
                        endtime=time_end,
                        tag="raw_recording")
    

samp_v = np.arange(0,len(pulser_rev),1)    
plt.plot(samp_v,dat_2[0].data, 'k')    

dat_2.filter("lowpass", freq=10.0)  
plt.plot(samp_v,dat_2[0].data, 'g')    




# Plotting the sample hammer signal

plt.plot(samp_v,pulser_rev, 'r')    
#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser)


    
# Here we check if the signal to compare, have the same sice, if not --> trimmed
pulser_actual = dat_2[0].data
if len(pulser_actual) > len(pulser_rev):
    pulser_actual = dat_2[0].data[0:len(pulser_rev)]
elif len(pulser_actual) < len(pulser_rev):
    pulser_rev = pulser_rev[0:len(dat_2[0].data)]
    samp_v = samp_v[0:len(dat_2[0].data)]

#plt.plot(samp_v,pulser_rev, 'r')    
#plt.plot(samp_v,pulser_actual, 'b')    






#hammer_corr = np.sum(hammer_rev - hammer_actual)
cross_corr = np.corrcoef(pulser_rev,pulser_actual)[0,1]
y_cut_rev = np.polyfit(samp_v, pulser_rev, 1)[1]    
y_cut_actual = np.polyfit(samp_v, pulser_actual, 1)[1]

print(cross_corr)

#pulser_start = [x for x in pulser_start if str(x) != 'nan']
#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser)


#    print('cross_corr:', cross_corr)

#if cross_corr <  0.8:
#    print('at: ', pulser_start[i], ' pulser deleted because of crosscor')
#    pulser_start[i] = 'nan'
#    
#if y_cut_actual <  2000 or y_cut_actual > 2500:
#    print('at: ', pulser_start[i], ' pulser deleted because of y cut')
#    pulser_start[i] = 'nan'
#
### Delets all "NaN" in hammer_hit_start vector and you'll be fine...;)
#pulser_start = [x for x in pulser_start if str(x) != 'nan']
#plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser)
