def get_pulser(dataStream):

    import numpy as np
    from obspy.signal.trigger import trigger_onset

    
    
    
    trigger_on = 3200
    trigger_off = 3200
#    on_of_1 = []

    on_of = trigger_onset(dataStream[0].data, trigger_on, trigger_off)
    
    if len(on_of) ==0: # Checks if something gets picked
        on_of_start_times = []
    
    else:
        on_of = on_of.flatten()
        on_of = np.unique(on_of) # throughs dublicates out
        

    if on_of[0] == 0:
        on_of = np.delete(on_of, [0])
    if on_of[-1] == (dataStream[0].stats.npts -1):
        on_of = on_of[:-1]

    on_of_start_times = [dataStream[0].stats.starttime + (x-1)*1/dataStream[0].stats.sampling_rate for x in on_of]


    return on_of_start_times
    del on_of_start_times
