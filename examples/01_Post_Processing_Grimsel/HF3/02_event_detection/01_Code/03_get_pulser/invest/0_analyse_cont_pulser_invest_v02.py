#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 11 10:30:10 2017

@author: linus
"""

import numpy as np
import matplotlib.pyplot as plt
from obspy.core import UTCDateTime, Stream
import os
import pyasdf
from c_get_pulser_v02 import get_pulser
import pandas as pd
from plot_waveform_pulser_invest_v01 import plot_waveform
from pprint import pprint

## Initialisation
data_path_1 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/1_ASDF' # Location of .h5 file
data_path_2 = '/media/linus/BACKUP_TILT/Linus/2_Hydraulic_fracturing/HF8/2_event_detection/01_Code/00_log'
if not os.path.exists(data_path_2):
    os.makedirs(data_path_2)

os.chdir(data_path_2)

# Pulser signal reference
pulser_ref = np.load('pulser_signal_02s_before.npy')
samp_v = np.arange(0,len(pulser_ref),1) # sample vector to ref. pulser signal
y_cut_rev = np.polyfit(samp_v, pulser_ref, 1)[1] # y_cut of ref. pulser signal    
#np.save('pulser_signal.npy', dat_2[0].data)  


os.chdir(data_path_1)
dat_1 = pyasdf.ASDFDataSet("20170518_HF6_HF8.h5")
os.chdir(data_path_2)
time_overlapp = 1 # Number of Overapp [s]    
time_start_1 = UTCDateTime(2017, 5, 18, 12, 30, 0, 000000)
delta_load_1= 60# Time in s
time_delta_1 = time_start_1 + delta_load_1
time_end_1 = UTCDateTime(2017, 5, 18, 16, 30, 0, 000000)

log_file = '2_Pulser_HF8.csv'
exp = 'HF8'

## Scheduling Pulser exeptions
os.chdir(data_path_2)
pulser_exep = pd.read_csv('2_Pulser_exeption_' + exp + '.csv')
pulser_exep = pulser_exep['Start'].tolist() + pulser_exep['End'].tolist()
for i in range(len(pulser_exep)):
    pulser_exep[i] = UTCDateTime(pulser_exep[i])
pulser_exep = np.reshape(pulser_exep,(2, len(pulser_exep)/2)).T

# actual scheduling
def time_inbetween(time_start,time_delta,inbetween):
    puls_exep = []
    for k in range(len(inbetween)):
        if time_start <= inbetween[k][0] <= time_delta:
            puls_exep.append(True)
        else:
            puls_exep.append(False)
    true_puls_exep = np.where(puls_exep)
    return(true_puls_exep[0])


#ch_in = np.linspace(1, 32, num=32)
ch_in = [1]

num = np.zeros(32)
dat_2 = []
pulser = []
pulser_start = []
pulser_end = []
pulser_raw = []
l = 0
while (time_delta_1 < time_end_1):
    dat_2 = Stream()
    if_int = time_inbetween(time_start_1,time_delta_1,pulser_exep) # If multible events are within the time span, first in list is taken (if_int[0])
    
    if len(if_int)==0: # Checks if list is empty
        start_time = (time_start_1 - time_overlapp)
        print('time start: ', start_time)
        print('time end: ', time_delta_1)                
        for k in range(len(ch_in)): 
            dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                              location="001", channel="001",
                              starttime=start_time , 
                                endtime=time_delta_1,
                                tag="raw_recording")
                
        pulser_start += get_pulser(dat_2)       
        
        time_start_1 += delta_load_1
        time_delta_1 += delta_load_1
            
        del dat_2

    else: # This comes into play as soon as 
        time_delta_1 = pulser_exep[int(if_int[0])][0] # If multible events are within the time span, first in list is taken (if_int[0])
        print('time start: ', time_start_1)
        print('time end: ', time_delta_1)
        for k in range(len(ch_in)): 
            dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                              location="001", channel="001",
                              starttime=time_start_1 , 
                                endtime=time_delta_1,
                                tag="raw_recording")
    
        pulser_start += get_pulser(dat_2)       
        
        time_start_1 = pulser_exep[0][1]
        time_delta_1 = pulser_exep[0][1] + delta_load_1
            
        del dat_2
    
    
## 2. Corelate pulser signal, and get y cut

for i in range(len(pulser_start)):
    time_before = 0.2 #Like this we identify falling slope of pulser
    time_start_2 = pulser_start[i] - time_before
    delta_2 = 3 # Time in s
    time_end_2 = time_start_2 + delta_2
    
    dat_2 = Stream()
    for k in range(len(ch_in)): 
        dat_2 += dat_1.get_waveforms(network="GRM", station=str(int(ch_in[k])).zfill(3),
                          location="001", channel="001",
                          starttime=time_start_2, 
                            endtime=time_end_2,
                            tag="raw_recording")
        
    dat_2.filter("lowpass", freq=10.0)  
    
    # Plotting the sample hammer signal
#        plt.plot(samp_v,pulser_ref, 'r')    
    #plot_waveform(dat_2, dat_2[0].stats.sampling_rate, 'Test', pulser)
    
    
        
    # Here we check if the signal to compare, have the same sice, if not --> trimmed
    pulser_actual = dat_2[0].data
    if len(pulser_actual) > len(pulser_ref):
        pulser_actual = dat_2[0].data[0:len(pulser_ref)]
    elif len(pulser_actual) < len(pulser_ref):
        pulser_ref = pulser_ref[0:len(dat_2[0].data)]
        samp_v = samp_v[0:len(dat_2[0].data)]
    
#        plt.plot(samp_v,pulser_ref, 'r')
#        plt.plot(samp_v,pulser_actual)    




    #hammer_corr = np.sum(hammer_rev - hammer_actual)
    cross_corr = np.corrcoef(pulser_ref,pulser_actual)[0,1]
    y_cut_actual = np.polyfit(samp_v, pulser_actual, 1)[1]
    
#    print('cross_corr:', cross_corr)
    
    if cross_corr <  0.8:
        print(pulser_start[i], ' pulser deleted because of crosscor')
        pulser_start[i] = 'nan'
        pass # if cross_cor not enough y cut does not get checked
        
    if y_cut_actual <  2000 or y_cut_actual > 2500:
        print('pulser deleted because of y cut', '(', i,'/',len(pulser_start),')')
        pulser_start[i] = 'nan'

## Delets all "nan" in pulser_start
pulser_start = [x for x in pulser_start if str(x) != 'nan']
print(len(pulser_start))

    
    
### Last manipulation    
pulser_start = list(np.unique(pulser_start))
pulser_del_vect = list(np.diff(pulser_start)) 
pulser_del = np.where((np.diff(pulser_start) <= 2)) # Checks where entries in pulser_start are too close

# Get ride of elements too close to each other
for f in range(len(pulser_del[0])):
    pulser_start[pulser_del[0][f]] = 'nan'
pulser_start = [x for x in pulser_start if str(x) != 'nan']

## Write log file
pulser_end = [None]*len(pulser_start)
survey = np.linspace(1, len(pulser_start), num=len(pulser_start))
survey = survey.tolist()
cols = pd.Index(['Pulser burst','Start','End'],name='cols')
for f in range(len(pulser_start)):
    pulser_start[f] = pulser_start[f] + 0.85 # Adding seconds to trig time
    pulser_end[f] = pulser_start[f] + 28.335 # Adding duration of pulser burst
#    pulser_start[f] = pulser_start[f].isoformat()
#    pulser_end[f] = pulser_end[f].isoformat()
    data = [survey[f]] + [pulser_start[f].isoformat()] + [pulser_end[f].isoformat()]                     
    df = pd.DataFrame(data = [data], columns = cols)


    os.chdir(data_path_2)
    if os.path.exists(log_file):
        df.to_csv(log_file, mode='a', header=False,index = False)
    else:
        df.to_csv(log_file,index = False)
    
    os.chdir(data_path_2)  

                
                
                
                
                