def get_pulser(dataStream):

    import numpy as np
    from obspy.signal.trigger import trigger_onset

    
#    trigger_on = 3200
#    trigger_off = 3200
#    
    trigger_on = 3150
    trigger_off = 3150
#    on_of_1 = []

    on_of = trigger_onset(dataStream[0].data, trigger_on, trigger_off)
    
    if len(on_of) == 0: # Checks if something gets picked
        on_of_start_times = []
        pass
    
    else:
        on_of = on_of.flatten()
        on_of = np.unique(on_of) # throughs duplicates out
        

    if len(on_of) == 0:
        pass
    elif on_of[0] == 0: # Deletes first entry if zero
        on_of = np.delete(on_of, [0])
    elif on_of[-1] == (dataStream[0].stats.npts -1): # Deletes last entry
        on_of = on_of[:-1]

    on_of_start_times = [dataStream[0].stats.starttime + (x-1)*1/dataStream[0].stats.sampling_rate for x in on_of]


    return on_of_start_times
    del on_of_start_times
