function phi = phase2rayangle(eps,delta,vmin,theta)
%

% dtheta = 0.1/180*pi;
% theta = 0:dtheta:pi;
% theta = linspace(0,pi,1800);
% dtheta = mean(diff(theta));
v = vmin*(1 + delta*sin(theta).^2 .* cos(theta).^2 + eps*sin(theta).^4);
dv = vmin*(2*delta*sin(theta) .* cos(theta) .*(cos(theta).^2 - sin(theta).^2) + 4*eps*sin(theta).^3 .* cos(theta));
tt = (tan(theta) + dv./v)./(1 - tan(theta)./v.*dv);
phi = atan(tt);
ii = find(phi < 0);
phi(ii) = phi(ii) + pi;
phi(1) = theta(1);
phi(end) = theta(end);
% clf;
% subplot(311); plot(theta/pi*180,v);
% subplot(312); plot(theta/pi*180,dv,'r',theta/pi*180,dv2,'b');
% subplot(313); plot(theta/pi*180,(theta - phi)/pi*180);

