#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  7 09:51:30 2017

@author: linus
"""

## Pickertool for livemonitoring
import os, time
import numpy as np
import pandas as pd
from pick_mat_v01 import pick_mat

## Initializing
# Path
experiment = 'HF_picks'
data_path = '/home/linus/Desktop/20170518_HF/' # Directory of Experiment

data_path_1 = '/media/windowsshare/01_events' # Location of raw events
data_path_2 = data_path + '2_data/2_picks_python/1_events' # Location picked events
data_path_3 = data_path + '2_data/1_log_parm' # Location of log file and parameter file
data_path_4 = data_path + '2_data/2_picks_python/2_electric_noise' # Electric noise

# Get event-file names
mat_files = sorted([f for f in os.listdir(data_path_1) if f.endswith('.mat')]) # generates a list of the .dat files in data_path_1

# Load parameters for picker
os.chdir(data_path_3)
parm_STA_LTA = np.loadtxt('parm_STA_LTA_events.dat',delimiter=' ')

# Check if log file exists
log_file = experiment + '.csv'
if os.path.exists(log_file): # If it does exist, find last processed event in mat_files
    df = pd.read_csv(log_file)
    last_ev = df["Event_rec"].iloc[-1] + '.mat'  #Gets last processed event
    event_counts_csv = df["Event"].iloc[-1]
    index_last_ev = [item for item in range(len(mat_files)) if mat_files[item] == last_ev]
    mat_files = mat_files[(index_last_ev[0]+1):-1]
    event_count = event_counts_csv+1 
    # Process all .mat's in folder...
    # Transfer list of .mat's to before (in case new .mat's have arrived during processing)                                              
else:    
    event_count = 1
    pass

# Passive mode, works on files which are already in folder, but not yet processed
# due to an interuptoin of the program

for g in range(len(mat_files)):
    event_time, event_count, trig_time, bin_width, num_of_picks_needed = pick_mat(data_path_1, data_path_2, data_path_3, data_path_4, mat_files[g] , event_count,parm_STA_LTA,log_file)
    event_count = event_count + 1
    print(event_time)

## Surveillance        
path_to_watch = data_path_1
#before = sorted([f for f in os.listdir(path_to_watch) if f.endswith('.mat')])

# in case we have just one .mat file, we have to wirte before again
if not mat_files:
    before = mat_files = sorted([f for f in os.listdir(data_path_1) if f.endswith('.mat')]) # generates a list of the .dat files in data_path_1
else:
    pass

before = mat_files
while 1:
  time.sleep (1)
  after = sorted([f for f in os.listdir(path_to_watch) if f.endswith('.mat')])
  added = [f for f in after if not f in before]
#  removed = [f for f in before if not f in after]
  if added:
      time.sleep (0.5)
      list.sort(added)    
      for h in range(len(added)): 
          event_time, event_count, trig_time, bin_width, num_of_picks_needed = pick_mat(data_path_1, data_path_2, data_path_3, data_path_4, added[h] , event_count,parm_STA_LTA,log_file)
          event_count = event_count + 1
          print(event_time)
  before = after
    
        
        
        
        
        
        

