#  Celery App
#
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#

try:
    from celery import Celery

    celery_app = Celery(main='dug_seis')
    # Use previously generated config.
    celery_app.config_from_object('dug_seis.celeryconfig')

except ImportError:
    pass


