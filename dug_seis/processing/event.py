# Event class of DUG-Seis
#
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#



from obspy.core.event.event import Event as ObsPyEvent
from obspy.signal.trigger import recursive_sta_lta, trigger_onset
from obspy.core.event import WaveformStreamID, Pick, Origin, Arrival, Magnitude
import numpy as np
import pyproj
import pandas as pd
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt
import logging

# necesary for picker
import sys
sys.path.append("../")
from dug_seis.processing.Pickers.PhasePApy_Austin_Holland import fbpicker
from obspy.core import *
from dug_seis.processing.Pickers.PhasePApy_Austin_Holland import aicdpicker
from dug_seis.processing.Pickers.PhasePApy_Austin_Holland import ktpicker
from dug_seis.processing.Pickers.P_Phase_Picker_USGS.pphasepicker import pphasepicker

#
# this class contains the bandass, picker, localization, magntiude and visualization to be applied the 20ms waveform.
class Event(ObsPyEvent):
    def __init__(self, param, wf_stream, event_id, classification, logger):
        super().__init__(resource_id='smi:%s/event/%d' % (param['General']['project_name'], event_id))

        self.param = param
        self.prparam = param['Processing']
        self.wf_stream = wf_stream
        self.event_id = event_id
        self.classification = classification
        self.t0 = []
        self.dist = []
        self.tcalc = []
        self.loc_ind = []
        self.logger = logger
        if self.classification != 'noise':
            t_start_plot = self.wf_stream.traces[0].stats["starttime"]
            self.logger.info('Event ' + str(self.event_id) + ': Time ' + str(t_start_plot)[11:-1] + ', processing started.')
        else:
            t_start_plot = self.wf_stream.traces[0].stats["starttime"]
            self.logger.info('Noise Visualisation at ' + str(t_start_plot)[11:-1] + ', processing started.')

    # apply a bandpass filter to all 32 traces.
    def bandpass(self):
        # bandpass filter using same parameters as for triggering
        self.wf_stream.filter("bandpass", freqmin=self.prparam['bandpass_f_min'], freqmax=self.prparam['bandpass_f_max'])


    # apply a picker algorithm on all 32 traces to pick first arrivals.
    def pick(self):
        piparam = self.prparam['Picking']
        df = self.wf_stream[0].stats.sampling_rate

        # apply the fb picker.
        if piparam['algorithm'] == 'fb':
            t_long = 5 / 1000
            freqmin = 1
            mode = 'rms'
            t_ma = 20 / 1500
            nsigma = 8/100
            t_up = 0.4 / 100
            nr_len = 2
            nr_coeff = 2
            pol_len = 10
            pol_coeff = 10
            uncert_coeff = 3
            picker = fbpicker.FBPicker(t_long=t_long, freqmin=freqmin, mode=mode, t_ma=t_ma, nsigma=nsigma, t_up=t_up,
                                       nr_len=nr_len, nr_coeff=nr_coeff, pol_len=pol_len, pol_coeff=pol_coeff,
                                       uncert_coeff=uncert_coeff)

            for j in range(len(self.wf_stream)):  # do picking for each trace in 20ms snippet
                tr = self.wf_stream[j]
                tr.detrend('linear')  # Perform a linear detrend on the data

                scnl, picks, polarity, snr, uncert = picker.picks(tr)

                if len(picks):
                    # t_picks=trig[0][0] / df  # if a pick is done on a trace, convert it to seconds
                    t_pick_UTC = picks[0]  # self.wf_stream[0].stats[
                    # "starttime"] + t_picks  # and add the start time of the 20ms snippet
                    station_id = j + 1
                    self.picks.append(Pick(time=t_pick_UTC,
                                           resource_id='%s/picks/%d' % (self.resource_id.id, len(self.picks) + 1),
                                           waveform_id=WaveformStreamID(network_code='GR',
                                                                        station_code='%03i' % station_id,
                                                                        channel_code='001', location_code='00'),
                                           method_id='AICD'))

            logging.info('Event ' + str(self.event_id) + ': ' + str(len(self.picks)) + ' picks.')

        # apply the kt (kurtosis) picker.
        if piparam['algorithm'] == 'kt':
            t_win = 1/2000
            t_ma = 10/2000
            nsigma = 6/100
            t_up = 0.78/100
            nr_len = 2
            nr_coeff = 2
            pol_len = 10
            pol_coeff = 10
            uncert_coeff = 3
            chenPicker = ktpicker.KTPicker(t_win=t_win, t_ma=t_ma, nsigma=nsigma, t_up=t_up, nr_len=nr_len,
                                           nr_coeff=nr_coeff, pol_len=pol_len, pol_coeff=pol_coeff, uncert_coeff=uncert_coeff)


            for j in range(len(self.wf_stream)):  # do picking for each trace in 20ms snippet
                tr = self.wf_stream[j]
                tr.detrend('linear')  # Perform a linear detrend on the data

                scnl, picks, polarity, snr, uncert = chenPicker.picks(tr)


                if len(picks):

                    t_pick_UTC = picks[0]
                    station_id = j + 1
                    self.picks.append(Pick(time=t_pick_UTC,
                                           resource_id='%s/picks/%d' % (self.resource_id.id, len(self.picks) + 1),
                                           waveform_id=WaveformStreamID(network_code='GR',
                                                                        station_code='%03i' % station_id,
                                                                        channel_code='001', location_code='00'),
                                           method_id='AICD'))

            logging.info('Event ' + str(self.event_id) + ': ' + str(len(self.picks)) + ' picks.')

        # apply the AICD picker.
        if piparam['algorithm'] == 'aicd':
            t_ma = 3 / 1000
            nsigma = 8
            t_up = 0.78 / 1000
            nr_len = 2
            nr_coeff = 2
            pol_len = 10
            pol_coeff = 10
            uncert_coeff = 3
            chenPicker = aicdpicker.AICDPicker(t_ma=t_ma, nsigma=nsigma, t_up=t_up, nr_len=nr_len, nr_coeff=nr_coeff,
                                               pol_len=pol_len, pol_coeff=pol_coeff,
                                               uncert_coeff=uncert_coeff)

            picksav=[]

            for j in range(len(self.wf_stream)):  # do picking for each trace in 20ms snippet
                tr = self.wf_stream[j]
                tr.detrend('linear')  # Perform a linear detrend on the data

                # scnl, picks, polarity, snr, uncert = picker.picks(tr)
                scnl, picks, polarity, snr, uncert = chenPicker.picks(tr)


                if len(picks):

                    t_pick_UTC = picks[0]
                    station_id = j + 1
                    picksav.append(t_pick_UTC-self.wf_stream[0].stats[
                                     "starttime"])
                    self.picks.append(Pick(time=t_pick_UTC,
                                           resource_id='%s/picks/%d' % (self.resource_id.id, len(self.picks) + 1),
                                           waveform_id=WaveformStreamID(network_code='GR',
                                                                        station_code='%03i' % station_id,
                                                                        channel_code='001', location_code='00'),
                                           method_id='AICD'))
            pickaverage=np.mean(picksav)*1000
            logging.info('Event ' + str(self.event_id) + ': ' + str(len(self.picks)) + ' picks.')

        # apply the STA LTA picker.
        if piparam['algorithm'] == 'sta_lta':
            for j in range(len(self.wf_stream)):  # do picking for each trace in 20ms snippet

                cft = recursive_sta_lta(self.wf_stream[j], piparam['sta_lta']['st_window'],
                                        piparam['sta_lta']['lt_window'])  # create characteristic function
                trig = trigger_onset(cft, piparam['sta_lta']['threshold_on'],
                                     piparam['sta_lta']['threshold_off'])  # do picking
                if len(trig):
                    t_picks = trig[0][0] / df  # if a pick is done on a trace, convert it to seconds
                    t_pick_UTC = self.wf_stream[0].stats[
                                     "starttime"] + t_picks  # and add the start time of the 20ms snippet
                    station_id = j + 1
                    self.picks.append(Pick(time=t_pick_UTC,
                                           resource_id='%s/picks/%d' % (self.resource_id.id, len(self.picks) + 1),
                                           waveform_id=WaveformStreamID(network_code='GR',
                                                                        station_code='%03i' % station_id,
                                                                        channel_code='001', location_code='00'),
                                           method_id='recursive_sta_lta'))

            logging.info('Event ' + str(self.event_id) + ': ' + str(len(self.picks)) + ' picks.')

        # apply the P Phase picker.
        if piparam['algorithm'] == 'pphase':
            Tn = 0.01
            xi = 0.6

            for j in range(len(self.wf_stream)):  # do picking for each trace in 20ms snippet

                loc = pphasepicker(self.wf_stream[j],Tn,xi)
                if loc != 'nopick':
                    t_picks = loc
                    t_pick_UTC = self.wf_stream[0].stats[
                                     "starttime"] + t_picks  # and add the start time of the 20ms snippet
                    station_id = j + 1
                    self.picks.append(Pick(time=t_pick_UTC,
                                           resource_id='%s/picks/%d' % (self.resource_id.id, len(self.picks) + 1),
                                           waveform_id=WaveformStreamID(network_code='GR',
                                                                        station_code='%03i' % station_id,
                                                                        channel_code='001', location_code='00'),
                                           method_id='recursive_sta_lta'))

            logging.info('Event ' + str(self.event_id) + ': ' + str(len(self.picks)) + ' picks.')

    # apply a localization algorithm that makes use of the picks done by the picker module.
    def locate(self):
        lparam = self.prparam['Locate']
        if lparam['algorithm'] == 'hom_aniso':
            aparam = lparam['hom_aniso']

        # apply localization if there is a minimum number of picks done for the event.
        if len(self.picks) < lparam['min_picks']:
            return

        trig_ch = []
        tobs = []
        for i in self.picks:
            if np.max(np.abs(self.param['General']['sensor_coords'][int(i.waveform_id['station_code'])-1, :])):
                trig_ch.append(int(i.waveform_id['station_code']) - 1)
                tobs.append((i.time - self.wf_stream.traces[0].stats["starttime"]) * 1000)
        npicks = len(tobs)
        sensor_coords = self.param['General']['sensor_coords'][trig_ch, :]
        vp = self.prparam['vp'] * np.ones([npicks]) / 1000.

        if lparam['algorithm'] == 'hom_iso' or lparam['algorithm'] == 'hom_aniso':
            loc = sensor_coords[tobs.index(min(tobs)), :] + 0.1
            t0 = min(tobs)
            nit = 0
            jacobian = np.zeros([npicks, 4])
            dm = 1. * np.ones(4)

            while nit < 100 and np.linalg.norm(dm) > 0.00001:
                nit = nit + 1

                if lparam['algorithm'] == 'hom_aniso':  # calculate anisotropic velocities
                    for i in range(npicks):
                        azi = np.arctan2(sensor_coords[i, 0] - loc[0], sensor_coords[i, 1] - loc[1])
                        inc = np.arctan2(sensor_coords[i, 2] - loc[2], np.linalg.norm(sensor_coords[i,range(2)]-loc[range(2)]))
                        theta = np.arccos(np.cos(inc) * np.cos(azi) * np.cos(aparam['inc']) * np.cos(aparam['azi']) +
                                          np.cos(inc) * np.sin(azi) * np.cos(aparam['inc']) * np.sin(aparam['azi']) +
                                          np.sin(inc) * np.sin(aparam['inc']))
                        vp[i] = aparam['vp_min'] / 1000. * (1.0 + aparam['delta'] * np.sin(theta)**2 * np.cos(theta)**2 +
                                                            aparam['epsilon'] * np.sin(theta)**4)

                dist = [np.linalg.norm(loc - sensor_coords[i, :]) for i in range(npicks)]
                tcalc = [dist[i] / vp[i] + t0 for i in range(npicks)]

                res = [tobs[i] - tcalc[i] for i in range(npicks)]
                rms = np.linalg.norm(res) / npicks
                for j in range(3):
                    for i in range(npicks):
                        jacobian[i, j] = -(sensor_coords[i, j] - loc[j]) / (vp[i] * dist[i])
                jacobian[:, 3] = np.ones(npicks)

                dm = np.matmul(np.matmul(np.linalg.inv(np.matmul(np.transpose(jacobian), jacobian) +
                               pow(lparam['damping'], 2) * np.eye(4)), np.transpose(jacobian)), res)
                loc = loc + dm[0:3]
                t0 = t0 + dm[3]
        else:
            self.logger.warning('Location algorithm not implemented. Location not estimated.')

        x = loc[0] + self.param['General']['origin_ch1903_east']
        y = loc[1] + self.param['General']['origin_ch1903_north']
        z = loc[2] + self.param['General']['origin_elev']
        t0 = self.wf_stream.traces[0].stats["starttime"] + t0 / 1000.

        # write CSV file with CH1903 coordinates
        cols = pd.Index(['Event_id', 'datenum', 'x', 'y', 'z'], name='cols')
        outdata = [self.event_id] + [t0.matplotlib_date] + [x] + [y] + [z]
        df = pd.DataFrame(data=[outdata], columns=cols)
        df.to_csv('event_loc.csv', mode='a', header=False, index=False)

        # convert coordinates to WGS84
        proj_ch1903 = pyproj.Proj(init="epsg:21781")
        proj_wgs84 = pyproj.Proj(init="epsg:4326")
        lon, lat = pyproj.transform(proj_ch1903, proj_wgs84, x, y)
        # create origin object and append to event
        o = Origin(time=t0,
                   longitude=lon, latitude=lat, depth=z,
                   resource_id='%s/origin/%d' % (self.resource_id.id, len(self.origins)+1),
                   depthType='elevation',
                   methodID=lparam['algorithm'])
        for i in self.picks:
            if self.param['General']['sensor_coords'][int(i.waveform_id['station_code'])-1, 0]:
                o.arrivals.append(Arrival(pick_id=i.resource_id,
                                          time_residual=res[len(o.arrivals)] / 1000,
                                          phase='p',
                                          resource_id='%s/origin/%d/arrival/%d' % (
                                          self.resource_id.id, len(self.origins)+1, len(o.arrivals))))
        o.time_errors = rms / 1000
        self.origins.append(o)
        self.preferredOriginID = o.resource_id
        self.extra = {'x': {'value': loc[0],
                            'namespace': 'http://sccer-soe.ch/local_coordinates/1.0'},
                      'y': {'value': loc[1],
                            'namespace': 'http://sccer-soe.ch/local_coordinates/1.0'},
                      'z': {'value': loc[2],
                            'namespace': 'http://sccer-soe.ch/local_coordinates/1.0'}}
        self.t0.append(t0)
        self.dist.append(dist)
        self.tcalc.append(tcalc)
        self.loc_ind.append(trig_ch)
        self.logger.info('Event ' + str(self.event_id) + ': Location %3.2f %3.2f %3.2f; %i iterations, rms %4.3f ms'
                     % (loc[0], loc[1], loc[2], nit, rms))

    # apply a magnitude estimation that makes use of the picks to find the maximum amplitude of arrivals.
    def est_magnitude(self, origin_number=np.inf):
        emparam = self.prparam['Magnitude']
        if origin_number > len(self.origins):
            origin_number = len(self.origins)
        elif origin_number < 1:
            self.logger.warning('Origin number does not exist.')
        origin_number = origin_number - 1

        trig_ch = []
        for i in self.picks:
            if self.param['General']['sensor_coords'][int(i.waveform_id['station_code']) - 1, 0]:
                trig_ch.append(int(i.waveform_id['station_code']))
        ch_in = [int(i.stats['station']) for i in self.wf_stream.traces]
        ind_trig = np.where(np.isin(ch_in, trig_ch))  # Finds correct axis

        if len(self.origins):
            self.ts_approx = []
            t0_orig = self.t0[origin_number] - self.wf_stream.traces[0].stats["starttime"]  # t0 for use in magnitude estimation
            righthand = []
            for i in range(len(self.dist[origin_number])):
                self.ts_approx.append((self.dist[origin_number][i] / self.prparam['vs'] + t0_orig) * 1000)  # time of s wave arrival/end of the segment (time)
                righthand.append((self.dist[origin_number][i] / self.prparam['vs'] -
                                  self.dist[origin_number][i] / self.prparam['vp']) * 1000)
                if self.ts_approx[i] > (self.param['Trigger']['endtime']+self.param['Trigger']['endtime'])*1000:
                    self.ts_approx[i] = (self.param['Trigger']['endtime']+self.param['Trigger']['endtime'])*1000

            relstartwindow = []

            #we create a time window with a begin and end time around the expected maximum amplitude of the arrival of the event at each trace.
            #this time window is based on the pick from the localization module(ts_approx)
            for i in range(len(self.ts_approx)):
                relstartwindow.append(self.ts_approx[i] - 2 * righthand[i])  # begin of the segment (in time)
                if relstartwindow[i] < 0:
                    relstartwindow[i] = 0

            sample_start = [int(round(i / 1000 * self.wf_stream.traces[0].stats.sampling_rate)) for i in
                            relstartwindow]  # beginning of segment (in samples)
            sample_end = [int(round(i / 1000 * self.wf_stream.traces[0].stats.sampling_rate)) for i in
                          self.ts_approx]  # end of segment (in samples)

            maxamp = []

            for k in range(len(ind_trig[0])):

                maxamp.append(max(abs(self.wf_stream[ind_trig[0][k]].data[
                                      sample_start[k]:sample_end[k]])))  # find maximum amplitude in segment which will impact the magnitude estimation.

            corr_fac = []
            mag_exp= []
            for i in range(len(self.dist[origin_number])):

                corr_fac.append(np.exp(np.pi * (self.dist[origin_number][i] - emparam['r0']) * emparam['f0'] / (emparam['q'] * self.prparam['vp'])))
                mag_exp.append(maxamp[i] * self.dist[origin_number][i] / emparam['r0'] * corr_fac[i])
                # calculate relative magnitude for each receiver/event recording
            average_magnitude = np.log10(np.mean(mag_exp))

            self.logger.info('Event ' + str(self.event_id) + ': Relative magnitude %4.2f.' % average_magnitude)
            if(average_magnitude < np.inf):
                self.magnitudes.append(Magnitude(mag=average_magnitude,
                                                 resource_id='%s/magnitude/%d' % (
                                                 self.resource_id.id, len(self.magnitudes)+1),
                                                 origin_id=self.origins[origin_number].resource_id,
                                                 type='relative_amplitude'))
                self.preferredMagnitudeID = self.magnitudes[-1].resource_id

    # generates a visualization of the 32 traces in the 20ms waveform and the picks done by the picker module as well as the picks resulting from visualization.
    def event_plot(self, save_fig=False):
        fparam = self.prparam['Folders']

        # for name of each saved png, use start and end time of the 20ms snippet
        t_start_plot = self.wf_stream.traces[0].stats["starttime"]
        t_start_plot=str(t_start_plot)
        t_start_plotn = t_start_plot[11:-1]
        if self.classification=='noise':
            t_title = 'Noise Visualisation at ' + t_start_plotn
        else:
            t_title = 'Event ' + str(self.event_id) + ' at ' + t_start_plotn

        sampRate = self.wf_stream.traces[0].stats.sampling_rate
        time_v = np.arange(0, self.wf_stream[0].data.shape[0], 1) * 1 / sampRate * 1000
        a_max = []
        for k in range(len(self.wf_stream)):
            a_max.append(format(np.amax(np.absolute(self.wf_stream[k].data)), '.1f'))
        fig, axs = plt.subplots(len(self.wf_stream), 1, figsize=(20, 10), facecolor='w', edgecolor='k')

        plt.interactive(False)
        fig.subplots_adjust(hspace=0, wspace=0)

        axs = axs.ravel()

        for i in range(len(self.wf_stream)):
            axs[i].plot((time_v), self.wf_stream[i].data)
            axs[i].set_yticklabels([])
            axs[i].set_ylabel(self.wf_stream.traces[i].stats['station'] + '  ', rotation=0)
            axs[i].set_xlim([0, (len(time_v) / self.wf_stream[0].stats.sampling_rate) * 1000])

            ax2 = axs[i].twinx()
            ax2.set_yticklabels([])
            ax2.set_ylabel('        ' + str(a_max[i]), rotation=0, color='g')

            if i < len(self.wf_stream) - 1:
                axs[i].set_xticklabels([])
        if self.classification != 'noise' and self.classification != 'electronic':
            trig_ch = [int(i.waveform_id['station_code']) for i in self.picks]
            ch_in = [int(i.stats['station']) for i in self.wf_stream.traces]
            # finding the correct axes(stations)
            ind_trig = np.where(np.isin(ch_in, trig_ch))  # Finds correct axis
            trig_time = [(i.time - self.wf_stream.traces[0].stats["starttime"]) * 1000 for i in self.picks]

            # plotting pick time
            for m in range(len(trig_time)):
                axs[ind_trig[0][m]].plot((trig_time[m], trig_time[m]),
                                         (axs[ind_trig[0][m]].get_ylim()[0], axs[ind_trig[0][m]].get_ylim()[1]),
                                         'r-', linewidth=1.0)

            if len(self.origins):
                loc_ind_trig = self.loc_ind[-1][:]  # why was this? if this line gives error switch between ind_trig[0] and self.loc_ind[-1][:]
                for m in range(len(loc_ind_trig)):
                    axs[loc_ind_trig[m]].plot(
                        (self.tcalc[len(self.origins) - 1][m], self.tcalc[len(self.origins) - 1][m]),
                        (axs[loc_ind_trig[m]].get_ylim()[0], axs[loc_ind_trig[m]].get_ylim()[1]),
                        'g--', linewidth=1.0)
                    if len(self.magnitudes):
                        axs[loc_ind_trig[m]].plot((self.ts_approx[m], self.ts_approx[m]),
                                                  (axs[loc_ind_trig[m]].get_ylim()[0],
                                                   axs[loc_ind_trig[m]].get_ylim()[1]),
                                                  'y--', linewidth=1.0)


        plt.suptitle(t_title, fontsize=17)
        fig.text(0.49, 0.035, 'time [ms]', ha='center', fontsize=14)
        fig.text(0.040, 0.5, 'channel []', va='center', rotation='vertical', fontsize=14)
        fig.text(.988, 0.5, 'peak amplitude [mV]', va='center', rotation=90, fontsize=14, color='g')
        # plt.show()

        if self.classification != 'noise':
            if save_fig:
                fig.savefig(fparam['plot_folder_'+self.classification] +
                            "/" + t_start_plot.replace(':', '').replace('.', '_').replace('T', '_').replace('-', '')[:-3] +"_" + "{:03d}".format(self.event_id) + ".png", dpi=100)
                self.logger.info('Event ' + str(self.event_id) + ': Plotted, Figure saved.')
            else:
                self.logger.info('Event ' + str(self.event_id) + ': Plotted, Figure not saved.')
        else:
            if save_fig:

                fig.savefig(fparam['noise_vis_folder'] + "/noise_vis-" +  t_start_plotn.replace(':', '_') + ".png", dpi=100)
                self.logger.info('Noise Visualisation: Plotted, Figure saved.')

            else:
                self.logger.info('Noise Visualisation: Plotted, Figure not saved.')
        plt.close(fig)

    def event_save_csv(self, filename='events.csv'):
        # write CSV file with CH1903 coordinates
        cols = pd.Index(['Event_id', 'Date&Time', 'x', 'y', 'z', 'Mag', 'loc_rms', 'npicks'], name='cols')
        if len(self.t0):
            t0 = self.t0[-1]
            x = self.extra['x']['value']
            y = self.extra['y']['value']
            z = self.extra['z']['value']
            rms = self.origins[-1].time_errors['uncertainty']
        else:
            t0 = '                 '
            x, y, z, rms = 0, 0, 0, 0
        if len(self.magnitudes):
            Mag = self.magnitudes[-1]['mag']
        else:
            Mag = -99
        npicks = len(self.picks)
        outdata = [self.event_id] + [t0] + [np.round(x, 3)] + [np.round(y, 3)] + [np.round(z, 3)] + [np.round(Mag,2)] + [np.round(rms,5)] + [npicks]
        df = pd.DataFrame(data=[outdata], columns=cols)
        df.to_csv(filename, mode='a', header=False, index=False)
        self.logger.info('Event ' + str(self.event_id) + ': Info saved to %s.', filename)

    def event_save_ASDF(self):
        self.wf_stream.write('event' + str(self.event_id), 'H5')  # declare 'H5' as format
        print(read('event' + str(self.event_id) + '.h5'))

