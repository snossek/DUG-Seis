#  Processing module of DUG-Seis
#
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#




from dug_seis.processing.event import Event
from dug_seis.processing.get_waveforms import get_waveforms
from obspy import read


def event_processing(param, load_file, trig_time,  event_id, classification, logger):
    # get the new (standard 20ms) waveform around the triggered time for all 32 channels by sending the name of the data snippet and the trigger time to the get waveforms script.
    wf_stream = get_waveforms(param, load_file, trig_time)

    # In case the user wants to create new asdf files for each 20ms waveform of all 32 channels.
    if param['Processing']['waveform_toASDF'] == True:
        wf_stream.write('event' + str(event_id), 'H5')  # declare 'H5' as format

    # which modules in the event class script and the order of them to be applied on the 20ms waveform can be specified here by changing the order
    # of  the event modules or commenting them.
    event = Event(param, wf_stream, event_id, classification, logger)
    if classification == 'noise':
        event.event_plot(save_fig=True)
    elif classification == 'electronic':
        event.bandpass()
        event.event_plot(save_fig=True)
    else:
        event.bandpass()
        # event.prparam['Picking']['algorithm'] = 'sta_lta'
        event.pick()
        event.locate()
        #event.est_magnitude()
        event.event_plot(save_fig=True)
        # event.event_save_ASDF()
        #event.prparam['Locate']['algorithm'] = 'hom_aniso'
        #event.locate()
        #event.est_magnitude()
        #event.est_magnitude(origin_number=1)
        #event.classification = 'active'
        #event.event_plot(save_fig=True)
        #classification = 'passive'

        if classification == 'passive':
            event.write('%s/event%s.xml' % (param['Processing']['Folders']['quakeml_folder'], event_id), 'quakeml', validate=True)
        elif classification == 'active':
            event.write('%s/hammer%s.xml' % (param['Processing']['Folders']['quakeml_folder'], event_id), 'quakeml',
                        validate=True)

        event.event_save_csv('events.csv')
        logger.info('Finished event processing for event %d' % event_id)
