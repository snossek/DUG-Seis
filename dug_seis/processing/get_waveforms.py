#  Load waveform data for processing
#
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#

from obspy import Stream
import pyasdf
import numpy as np
import logging


def get_waveforms(param, load_file, trig_time):
    gparam = param['General']
    asdf_folder = param['Trigger']['asdf_folder']
    start_time = trig_time - param['Trigger']['starttime']
    end_time = trig_time + param['Trigger']['endtime']

    ch_in = range(1, gparam['sensor_count' ] +1)
    r = range(len(ch_in))

    tparam = param['Trigger']
    stations = [i - 1 for i in tparam['channels']]
    if param['General']['active_trigger_channel'] and param['General'][
        'active_trigger_channel'] - 1 not in stations:
        stations.insert(0, param['General']['active_trigger_channel'] - 1)
    stations.sort()

    # To create the 20ms waveform, load 2 data snippets if event in overlap otherwise load 1
    if len(load_file) == 1:
        wf_stream = Stream()
        ds = pyasdf.ASDFDataSet(asdf_folder + '/' + load_file[0], mode='r')

        for k in r:
            wf_stream += ds.get_waveforms(network=gparam['stats']['network'], station=str(int(ch_in[k])).zfill(3), location='00',
                                          channel='001',
                                          starttime=start_time,
                                          endtime=end_time,
                                          tag="raw_recording")


        if tparam['Gainrange'] == 'YAML':
            for k in r:
                # print(np.amax(np.absolute(wf_stream.traces[k].data)))
                wf_stream.traces[k].data = wf_stream.traces[k].data / 32768 * \
                                           param['Acquisition']['hardware_settings']['gain_selection'][k]
            logging.info('Gain range event retrieved from YAML file')
  
    else:
        # In this case the 20ms waveform will be loaded in from 2 data snippets.
        ds1 = pyasdf.ASDFDataSet(asdf_folder + '/' + load_file[0], mode='r')
        ds2 = pyasdf.ASDFDataSet(asdf_folder + '/' + load_file[1], mode='r') #GR_001

        if end_time > ds2.waveforms[gparam['stats']['network'] + '_001'].raw_recording.traces[0].stats["starttime"]:
            sta_p1 = Stream()
            sta_p2 = Stream()
            for k in r:
                sta_p1 += ds1.get_waveforms(network=gparam['stats']['network'], station=str(int(ch_in[k])).zfill(3), location='00',
                                            channel='001',
                                            starttime=start_time,
                                            endtime=ds1.waveforms[gparam['stats']['network'] + '_001'].raw_recording.traces[0].stats["endtime"],
                                            tag="raw_recording")
                sta_p2 += ds2.get_waveforms(network=gparam['stats']['network'], station=str(int(ch_in[k])).zfill(3), location='00',
                                            channel='001',
                                            starttime=ds2.waveforms[gparam['stats']['network'] + '_001'].raw_recording.traces[0].stats["starttime"],
                                            endtime=end_time,
                                            tag="raw_recording")
                wf_stream = sta_p1 + sta_p2
                wf_stream.merge(method=1, interpolation_samples=0)
            
            if tparam['Gainrange'] == 'YAML':
                for k in r:
                    wf_stream.traces[k].data = wf_stream.traces[k].data / 32768 * \
                                               param['Acquisition']['hardware_settings']['gain_selection'][k]
                logging.info('Gain range event retrieved from YAML file')

        else:
            wf_stream = Stream()
            if ds1.waveforms[gparam['stats']['network'] + '_001'].raw_recording.traces[0].stats["endtime"] < end_time:
                end_time = ds1.waveforms[gparam['stats']['network'] + '_001'].raw_recording.traces[0].stats["endtime"]
            for k in r:
                wf_stream += ds1.get_waveforms(network=gparam['stats']['network'], station=str(int(ch_in[k])).zfill(3), location='00',
                                               channel='001',
                                               starttime=start_time,
                                               endtime=end_time,
                                               tag="raw_recording")
            if tparam['Gainrange'] == 'YAML':
                for k in r:
                    wf_stream.traces[k].data = wf_stream.traces[k].data / 32768 * \
                                               param['Acquisition']['hardware_settings']['gain_selection'][k]
                logging.info('Gain range event retrieved from YAML file')
                
    return wf_stream



