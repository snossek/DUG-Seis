#  Trigger and processing module of DUG-Seis
# :copyright:
#    ETH Zurich, Switzerland
# :license:
#    GNU Lesser General Public License, Version 3
#    (https://www.gnu.org/copyleft/lesser.html)
#


import logging
from obspy import Stream
import os
import sys
import time
import pyasdf
from dug_seis.processing.dug_trigger import dug_trigger
from dug_seis.processing.event_processing import event_processing
import re


def processing(param):
    logger = logging.getLogger('dug-seis')
    logger.info('Starting trigger module')
    tparam = param['Trigger']
    if param['Processing']['parallel_processing']:
        if sys.platform == 'win32':
            os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')
            os.system('start redis-server > redis.log')
            os.system('start celery -A dug_seis worker --loglevel=debug --concurrency=%i > celery.log' % param['Processing']['number_workers'])
        else:
            os.system('redis-server > redis.log &')
            os.system('celery -A dug_seis worker --loglevel=debug --concurrency=%i &> celery.log &' %param['Processing']['number_workers'])
        from dug_seis.processing.celery_tasks import event_processing_celery

    # create folders for processing if they are not specified in the YAML file.
    if not os.path.exists(param['Processing']['Folders']['quakeml_folder']):
        os.makedirs(param['Processing']['Folders']['quakeml_folder'])
    if not os.path.exists(param['Processing']['Folders']['plot_folder_active']):
        os.makedirs(param['Processing']['Folders']['plot_folder_active'])
    if not os.path.exists(param['Processing']['Folders']['plot_folder_passive']):
        os.makedirs(param['Processing']['Folders']['plot_folder_passive'])
    if not os.path.exists(param['Processing']['Folders']['plot_folder_electronic']):
        os.makedirs(param['Processing']['Folders']['plot_folder_electronic'])
    if tparam['noise_vis'] and not os.path.exists(param['Processing']['Folders']['noise_vis_folder']):
        os.makedirs(param['Processing']['Folders']['noise_vis_folder'])

    # Initialisation
    asdf_folder = tparam['asdf_folder']  # 'raw'  # Location of .h5 file
    stations = [i - 1 for i in tparam['channels']]

    if len(tparam['channels']) != 1:
        if param['General']['active_trigger_channel'] and param['General']['active_trigger_channel'] - 1 not in stations:
            stations.insert(0, param['General']['active_trigger_channel'] - 1)
    stations.sort()
    event_nr = 0
    event_nr_s = []
    next_noise_vis = 0

    sta_overlap = Stream()

    # print(int(str(tparam['Time'])[6:8]))
    # if tparam['processing time'] == True:

    # load list of ASDF snippets in folder.
    new_files1 = sorted([f for f in os.listdir(asdf_folder) if f.endswith('.h5')]) # generates a list of the .asdf files in asdf_folder
    number2=[]
    for j in range(len(new_files1)):
        f = new_files1[j]
        number = " "
        for i in re.findall('\d+', f)[0:-3]:
            number = number + i
        number2.append(int(number))
    condition=int(str(tparam['Time']).replace('_', ''))
    number_approved = sorted(i for i in number2 if i >= condition )
    new_files = []
    if number_approved:
        index_start_approved = number2.index(number_approved[0])
        new_files = new_files1[index_start_approved:]

    processed_files = []

    # Load in both the data snippet(sta) and a to be set overlap(sta_total).

    while 1:
        if len(new_files):
            current_file = new_files.pop(0)
            logger.info('Working on file ' + current_file)
            sta = Stream()

            ds = pyasdf.ASDFDataSet(asdf_folder + '/' + current_file, mode='r')

            wf_list = ds.waveforms.list()
            for k in stations:
                sta += ds.waveforms[wf_list[k]].raw_recording
            sta_copy = sta.copy()
            # no overlap, if more than 10 samples are missing
            if not len(sta_overlap):
                sta_total = sta_copy
            else:
                if sta.traces[0].stats["starttime"]-sta_overlap.traces[0].stats["endtime"] > 10./param['Acquisition']['hardware_settings']['sampling_frequency']:
                    sta_total = sta_copy
                    logger.info('Gap in waveform data found.')
                else:
                    sta_total = sta_overlap + sta_copy

            # Use merge statement for merging the data snippet and the overlap, sta and sta_overlap that together form the data snippet sta_total.
            for tr in sta_total:
                tr.stats.delta = sta_total[0].stats.delta
            sta_total.merge(method=1, interpolation_samples=0)

            # Apply gainrange to all traces in the data snippet
            if tparam['Gainrange']=='YAML':
                for k in range(0, len(sta_total.traces)):
                    sta_total.traces[k].data = sta_total.traces[k].data / 32768 * \
                                               param['Acquisition']['hardware_settings']['gain_selection'][stations[k]]
                logger.info('Gain range trigger retrieved from YAML file')

            logger.debug(sta_total)

            # Send the loaded snippets including overlap to the trigger script
            trigger_out, event_nr = dug_trigger(sta_total, tparam, event_nr, event_nr_s)  # run trigger function

            overlap = tparam['starttime'] + tparam['endtime'] #+ tparam['endtime']['sta_lta']['lt_window']/sta_total.stats.sampling_rate

            t_end = sta.traces[0].stats["endtime"]
            sta_overlap = sta.trim(t_end - overlap, t_end)

            #trigger_out = trigger_out[trigger_out['Classification'] == 'passive']
            logger.debug(trigger_out)

            event_id = [i for i in trigger_out['Event_id']]
            classification = [i for i in trigger_out['Classification']]

            trig_time=[i for i in trigger_out['Time'][trigger_out['Time'] <t_end-overlap]] #do not store times in overlap

            #send the name of the data snippet in which the event lies, the triggered time of the event, the event id and the event classification to the event processing script.
            for l in range(0, len(trig_time)):
                if trig_time[l] < sta_copy.traces[0].stats["starttime"] + param['Trigger']['starttime']:
                    load_file = [processed_files[-1], current_file]
                else:
                    load_file = [current_file]
                if param['Processing']['parallel_processing']:
                    event_processing_celery.delay(param, load_file, trig_time[l], event_id[l], classification[l])
                    logger.info('Event ' + str(event_id[l]) + ' at ' + str(trig_time[l]) + ' sent to parallel worker.')
                else:

                    event_processing(param, load_file, trig_time[l], event_id[l], classification[l], logger)  # run processing for each event

            # noise visualization
            if tparam['noise_vis']:
                if next_noise_vis == 0:
                    next_noise_vis = sta_copy.traces[0].stats["starttime"]+param['Trigger']['starttime']#start time of current file+starttime
                while next_noise_vis < sta_copy.traces[0].stats["endtime"]:#end of current file:
                    if next_noise_vis< sta_copy.traces[0].stats["starttime"] + param['Trigger']['starttime']:
                        load_file = [processed_files[-1], current_file]
                    else:
                        load_file = [current_file]
                    if param['Processing']['parallel_processing']:
                        event_processing_celery.delay(param, load_file, next_noise_vis, 0, 'noise')  # start processing to visualize noise
                        logger.info('Noise at ' + str(next_noise_vis) + ' sent to parallel worker.')
                    else:
                        event_processing(param, load_file, next_noise_vis, 0, 'noise', logger)  # start processing to visualize noise
                    next_noise_vis += tparam['noise_vis_interval']

            processed_files.append(current_file)

        else:

            #check if there are new data snippets that have not been processed yet.
            flist1 = sorted([f for f in os.listdir(asdf_folder) if f.endswith('.h5')])  # generates a list of the .asdf files in asdf_folder
            number2 = []
            for j in range(len(flist1)):
                f = flist1[j]
                number = " "
                for i in re.findall('\d+', f)[0:-3]:
                    number = number + i
                number2.append(int(number))
            number_approved = sorted(i for i in number2 if i >= condition)
            if number_approved:
                index_start_approved = number2.index(number_approved[0])
                flist = flist1[index_start_approved:]
                new_files = [f for f in flist if f not in processed_files]


            if not len(new_files):
                logger.info('Waiting for new files.')
                time.sleep(1)
